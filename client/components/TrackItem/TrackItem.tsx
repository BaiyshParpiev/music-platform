import React from 'react';
import {ITrack} from "../../store/types";
import {Card, Grid} from'@material-ui/core';
import styles from '../../styles/Track.module.scss';
import IconButton from "@material-ui/core/IconButton";
import {PlayArrow, Pause, Delete} from "@material-ui/icons";
import {useRouter} from "next/router";
import { useActions } from "../../hooks/useActions";

interface TrackItemProps {
    track: ITrack;
    active?: boolean;
}

const TrackItem:React.FC<TrackItemProps> = ({track, active= false}) => {
    const router = useRouter();
    const {playTrack, pauseTrack, setActive} = useActions();

    const play = (e) => {
      e.stopPropagation();
      setActive(track);
      playTrack()
    }

    return (
        <Card className={styles.track} onClick={() => router.push('/tracks/' + track._id)}>
            <IconButton onClick={e => e.stopPropagation()}>
                {active ?
                    <Pause/>
                    :
                    <PlayArrow/>
                }
            </IconButton>
            <img width={70} height={70} src={"http://localhost:5000/" + track.picture} alt={track.name + ' image'}/>
            <Grid container direction="column" style={{width: 200, margin: '0 20px'}}>
                <div>{track.name}</div>
                <div style={{fontSize: 12, color: 'gray'}}>{track.artist}</div>
            </Grid>
            {active && <div>/</div>}
            <IconButton style={{marginLeft: 'auto'}} onClick={e => e.stopPropagation()}>
                <Delete/>
            </IconButton>
        </Card>
    );
};

export default TrackItem;