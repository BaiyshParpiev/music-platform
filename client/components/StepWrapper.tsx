import React from 'react';
import {Container, Stepper, StepLabel, Step, Grid, Card} from "@material-ui/core";

interface StepWrapperProps {
    activeStep: number;
}
const steps = ['Data about track', 'Create image', 'Create track'];
const StepWrapper:React.FC<StepWrapperProps> = ({activeStep, children}) => {
    return (
        <Container>
            <Stepper activeStep={activeStep}>
              {steps.map((step, i) =>
                <Step
                  key={i}
                  completed={activeStep > i}
                >
                  <StepLabel>{step}</StepLabel>
                </Step>
              )}
            </Stepper>
          <Grid
            container
            justifyContent="center"
            style={{margin: '70px 0', height: 270}}
          >
            <Card
              style={{width: 600}}
            >
              {children}
            </Card>
          </Grid>
        </Container>
    );
};

export default StepWrapper;