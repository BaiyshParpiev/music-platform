import React from 'react';
import {ITrack} from "../../store/types";
import {Grid, Box} from "@material-ui/core";
import TrackItem from '../TrackItem/TrackItem';

interface TrackListProps {
    tracks: ITrack[];
}

const TrackList:React.FC<TrackListProps> = ({tracks}) => {
    return (
        <Grid container direct="column">
            <Box>
                {tracks.map(track => <TrackItem key={track._id} track={track}/>)}
            </Box>
        </Grid>
    );
};

export default TrackList;