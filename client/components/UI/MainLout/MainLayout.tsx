import React from "react";
import { Container } from "@material-ui/core";
import Navbar from "../../Navbar";
import Player from "../Player/Player";

const MainLayout: React.FC = ({ children }) => {
  return (
    <>
      <Navbar />
      <Container style={{ margin: "90px 0 0" }}>
        {children}
      </Container>
      <Player />
    </>
  );
};

export default MainLayout;