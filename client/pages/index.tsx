import React from 'react';
import MainLayout from '../components/UI/MainLout/MainLayout';

const Index = () => {
    return (
        <>
            <MainLayout>
                <div className="center">
                    <h1>Welcome to Music App</h1>
                    <h3>Here is all the best songs our century</h3>
                </div>
            </MainLayout>
            <style jsx>
                {`
                  .center {
                    margin-top: 150px;
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    justify-content: center;
                  }
                `}
            </style>
        </>
    );
};

export default Index;