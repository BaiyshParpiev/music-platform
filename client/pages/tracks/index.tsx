import React from 'react';
import {Card, Grid, Button, Box} from '@material-ui/core';
import MainLayout from "../../components/UI/MainLout/MainLayout";
import {useRouter} from "next/router";
import TrackList from '../../components/TrackList/TrackList';
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { NextThunkDispatch, wrapper } from "../../store";
import { fetchTracks } from "../../store/actions/trackActions";
import { GetServerSideProps } from 'next'

const Index = () => {
    const router = useRouter();
    const {tracks, error} = useTypedSelector(state => state.tracks);
    console.log(tracks)

    if(error) {
        return <MainLayout>
            <h1>{error}</h1>
        </MainLayout>
    }

    return (
        <>
            <MainLayout>
                <Grid container justifyContent="center">
                    <Card style={{width: '90%'}}>
                        <Box p={3}>
                            <Grid container justifyContent="space-between">
                                <h1>List of tracks</h1>
                                <Button
                                    onClick={() => router.push('/tracks/create')}
                                >
                                    Create new
                                </Button>
                            </Grid>
                        </Box>
                        <TrackList tracks={tracks}/>
                    </Card>
                </Grid>
            </MainLayout>
        </>
    );
};

export default Index;


export const getServerSideProps: GetServerSideProps = wrapper.getServerSideProps(store =>
   async () => {
        const dispatch = store.dispatch as NextThunkDispatch;
        await dispatch( await fetchTracks())
    });

