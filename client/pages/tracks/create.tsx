import React, { useState } from "react";
import StepWrapper from "../../components/StepWrapper";
import { Grid, Button, TextField} from "@material-ui/core";
import MainLayout from "../../components/UI/MainLout/MainLayout";
import FileUpload from "../../components/FileUpload/FileUpload";
import { useInput } from "../../hooks/useInput";
import axios from "axios";
import { useRouter } from "next/router";


const Create = () => {
  const router = useRouter();
  const [activeStep, setActiveStep] = useState(0);
  const [picture, setPicture] = useState(null);
  const [audio, setAudio] = useState(null);
  const name = useInput('');
  const artist = useInput('');
  const text = useInput('');

  const next = () => {
    if (activeStep !== 2) {
      setActiveStep(prev => prev + 1);
    }else{
      const formData = new FormData();
      formData.append('name', name.value);
      formData.append('text', text.value);
      formData.append('artist', artist.value);
      formData.append('audio', audio);
      formData.append('picture', picture);
      axios.post('http://localhost:5000/tracks', formData).then(() => router.push('/tracks')).catch(e => console.log(e))
    }
  };

  const back = () => {
    setActiveStep(prev => prev - 1);
  };
  return (
    <MainLayout>
      <StepWrapper activeStep={activeStep}>
        {activeStep === 0 &&
          <Grid container direction="column" style={{ padding: 20 }}>
            <TextField
              {...name}
              style={{ marginTop: 10 }}
              label="Name of your track"
            />
            <TextField
              {...artist}
              style={{ marginTop: 10 }}
              label="Author"
            />
            <TextField
              {...text}
              style={{ marginTop: 10 }}
              label="About track"
              multiline
              rows={3}
            />

          </Grid>}
        {activeStep === 1 &&
          <FileUpload setFile={setPicture} accept="image/">
            <Button>Download cover</Button>
          </FileUpload>
        }
        {activeStep === 2 &&
          <FileUpload setFile={setAudio} accept="audio/">
            <Button>Download audio</Button>
          </FileUpload>
        }
      </StepWrapper>
      <Grid container justifyContent="space-between">
        <Button disabled={activeStep === 0} onClick={back}>Back</Button>
        <Button onClick={next}>Next</Button>
      </Grid>
    </MainLayout>
  );
};

export default Create;