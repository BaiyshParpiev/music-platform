import React from 'react';
import MainLayout from "../../components/UI/MainLout/MainLayout";
import {Button, Grid} from "@material-ui/core";
import {useRouter} from '@next/router';

const TrackPage = ({track}) => {
    const router = useRouter();
    return (
        <MainLayout>
            <Button
                variant="outlined"
                style={{fontSize: 32}}
                onClick={() => router.push('/tracks')}
            >
                To list
            </Button>
            <Grid container style={{margin: '20px 0'}}>
                <img src={track.picture} alt={track.name} width={200} height={200}/>
                <div  style={{margin: '20px 0'}}>
                    <h1>Name of track: {track.name}</h1>
                    <h1>Author: {track.artist}</h1>
                    <h1>Count of listens: {track.listens}</h1>
                </div>
            </Grid>
            <h1>About track</h1>
            <p>{track.text}</p>
            <h1>Comments</h1>
            <Grid container>
                <TextFiield
                    label="Your name"
                    fullWidth
                />
                <TextFiield
                    label="Your comment"
                    fullWidth
                    multiline
                    rows={4}
                />
                <Button>
                    Send
                </Button>
            </Grid>
            <div>{
                track.comments.map(comment =>
                <div>
                    <div>Author - {comment.username}</div>
                    <div>Comment - {comment.text}</div>
                </div>)
            }</div>
        </MainLayout>
    );
};

    export default TrackPage;