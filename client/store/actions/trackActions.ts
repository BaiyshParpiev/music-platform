import { Dispatch } from "react";
import { TrackActionTypes, TracksActions } from "../types";
import axios from "axios";

export const fetchTracks = () => {
  return async(dispatch: Dispatch<TracksActions>) => {
    try{
      const { data } = await axios.get('http://localhost:5000/tracks');
      dispatch({type: TrackActionTypes.FETCH_TRACKS, payload: data});
    }catch(e){
      dispatch({type: TrackActionTypes.FETCH_TRACKS_ERROR, payload: "Something went wrong"})
    }
  }
}