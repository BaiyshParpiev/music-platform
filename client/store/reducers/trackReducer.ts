import { TrackActionTypes, TracksActions, TrackState } from "../types";

const initialState: TrackState = {
  tracks: [],
  error: ''
}
export const trackReducer = (state = initialState, action: TracksActions): TrackState => {
  switch (action.type){
    case TrackActionTypes.FETCH_TRACKS:
      return {...state, tracks: action.payload};
    case TrackActionTypes.FETCH_TRACKS_ERROR:
      return {...state, error: ''};
    default:
      return state;
  }
}