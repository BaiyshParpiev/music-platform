export class CreateDtoTrack {
  readonly name;
  readonly artist;
  readonly text;
}